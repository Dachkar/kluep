package edu.buffalo.cse116;

import players.Player;

public class Accusation {
	private Player accuser;
	private Card room;
	private Card weapon;
	private Card plyer;
	private static boolean isTrue;

	public Accusation(Player pl, Card romcard, Card plcard, Card weapcard) {
		accuser = pl;
		room = romcard;
		weapon = weapcard;
		plyer = plcard;
	}

	public Player getAccuser() {
		return accuser;
	}

	public void setAccuser(Player accuser) {
		this.accuser = accuser;
	}

	public Card getRoomCard() {
		return room;
	}

	public void setRoomCard(RoomCard room) {
		this.room = room;
	}

	public Card getWeaponCard() {
		return weapon;
	}

	public void setWeaponCard(WeaponCard card) {
		this.weapon = card;
	}

	public static boolean isTrue() {
		return isTrue;
	}

	public static void setTrue(boolean istrue) {
		isTrue = istrue;
	}

	public Card getPlyerCard() {
		return plyer;
	}

	public void setPlyerCard(PlayerCard plyer) {
		this.plyer = plyer;
	}

}
