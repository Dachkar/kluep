package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import exceptions.IllegalMoveExcetion;
import exceptions.IllegalPlayException;
import grid.Grid;
import players.Player;

public class Board {

	private ArrayList<Player> player_turn = new ArrayList<Player>();
	private boolean endGame;
	private Card[] wepCards = new WeaponCard[6];
	private Card[] playerCards = new PlayerCard[6];
	private Card[] room = new RoomCard[9];
	protected int playIndex;
	protected String name;

	public Board() throws IllegalMoveExcetion, IllegalPlayException {
		new Grid();
		/**
		 * get all the players
		 */
		int next = 0;
		ArrayList<Player> ar = Player.getThePlayers();
		for (int i = 0; i < ar.size(); i++) {
			next = ar.indexOf(ar.get(i));

		}
		// int next = ar.indexOf(arg0);

	}

	/**
	 * 
	 * @param ply
	 *            ply whose turn to move his token
	 * @throws IllegalMoveExcetion
	 *             IllegalMoveException is called a player move diagonally or
	 *             move passed the number of roll dice
	 * @throws IllegalPlayException
	 *             IllegalPlayException is called when a player mention in
	 *             accusation his/her token is not located
	 */
	public void startTheMove(Player ply) throws IllegalMoveExcetion, IllegalPlayException {
		System.out.println(ply.getName() + " is your turn to play");
		if (ply.makeAccusation(ply)) {
			if (accuse(ply)) {
				/**
				 * if accusation is proved true, end the game
				 */
				endGame = true;
			} else {
				Accusation.setTrue(false);
				boolean ac = Accusation.isTrue();
				ply.setAccusation(ac);
				/*
				 * if accusation is proved wrong, remove the player
				 */
				player_turn.remove(ply);
				/**
				 * then move token inside to in side a room to prevent any
				 * blockage
				 */
				ply.setTokenPosition(12, 3);
			}

		}
		if (ply.getTokenPosition() == 0 || ply.getTokenPosition() == 2) {
			System.out.println("You are now in a room you must make a suggestion");
			makeSuggestion(ply);
			if (ply.useSecretPassage(ply)) {
				ply.setTokenPosition(3, 10);

			}
		} else {
			// int rol1 = ply.getRollDie();
			// int rol2 = ply.getRollDie();
			// ply.setTokenPosition(rol1, rol2);

		}

	}

	/**
	 * 
	 * @param ply
	 *            ply the player to make a suggestion
	 */
	@SuppressWarnings("unused")
	private void makeSuggestion(Player ply) {
		ArrayList<Card> pcard = PlayerCard.getPlayerCards();
		ArrayList<Card> rcard = RoomCard.getRoomCards();
		ArrayList<Card> wcard = WeaponCard.getWeaponCards();

		String pName = pcard.get(2).getName();
		String rName = rcard.get(3).getName();
		String wName = wcard.get(1).getName();

		System.out.println("I, " + ply.getName() + " suggest that the " + "crime was commited in the " + rName + " by "
				+ pName + " with the " + wName);

		ArrayList<Player> pl = Player.getThePlayers();

		int index = pl.indexOf(ply);
		ArrayList<Player> allPlayers = new ArrayList<>();
		for (int i = 0; i < pl.size(); i++) {
			allPlayers.add(pl.get(i));
		}
		/**
		 * remove the player making the accusation
		 */

		if (index == allPlayers.size() - 1) {
			index = 0;
		}
		System.out.println(allPlayers.get(index).getName() + " is checking his cards");

		boolean suggestionIsTrue = true;

		while (allPlayers.get(index).equals(ply)) {
			System.out.println(allPlayers.get(index).getName() + " your turn to prove");
			System.out.println("looking player card:" + pcard.get(2).getCardName() + "\nLooking for room card:"
					+ rcard.get(3).getCardName() + "\nlooking wepon card:" + wcard.get(1).getCardName());

			suggestionIsTrue = checkSuggestion(allPlayers.get(index), pcard.get(2), rcard.get(3), wcard.get(1));

			index++;
			if (index > allPlayers.size()) {
				index = 0;
			}
			if (!suggestionIsTrue) {
				System.out.println("Some one has the cards");
				break;
			}

		}
	}

	/**
	 * 
	 * @param player
	 *            player's turn to prove the suggestion true/false
	 * @param roomCard
	 *            roomCard the room card mentioned in the suggestion
	 * @param weaponCard
	 *            weapon card mentioned in the suggestion
	 * @param pcard
	 *            pcard player card mentioned in the suggestion
	 * @return return true if suggestion is accurate, return false if otherwise
	 */
	@SuppressWarnings("static-access")
	private boolean checkSuggestion(Player player, Card roomCard, Card weaponCard, Card pcard) {

		List<Card> pcards = player.getPlayercards();
		int numOfMatchCards = 0;
		for (int i = 0; i < pcards.size(); i++) {
			if (pcards.get(i).getCardName().equals(weaponCard.getCardName())) {
				numOfMatchCards++;
			} else if (pcards.get(i).getCardName().equals(roomCard.getCardName())) {
				numOfMatchCards++;
			} else if (pcards.get(i).getCardName().equals(pcard.getCardName())) {
				numOfMatchCards++;
			}
			if (numOfMatchCards == 1) {
				System.out.println(player.getName() + " has " + numOfMatchCards + " matches");
				return false;
				/// show the card to the accuser only
			} else if (numOfMatchCards > 1) {
				System.out.println(player.getName() + " has " + numOfMatchCards + " matches");
				return false;
				// select only one to shown to the accuser
			}
		}

		return true;
	}

	/**
	 * 
	 * @param ply
	 *            ply is the accuser
	 * @return return true if accusation is prove to be true, false if otherwise
	 * @throws IllegalPlayException
	 *             IllegalPlayException is called when a player mention in
	 *             accusation his/her token is not located
	 */
	private boolean accuse(Player ply) throws IllegalPlayException {
		ArrayList<Card> pcard = PlayerCard.getPlayerCards();
		ArrayList<Card> rcard = RoomCard.getRoomCards();
		ArrayList<Card> wcard = WeaponCard.getWeaponCards();

		Accusation ac = new Accusation(ply, rcard.get(2), pcard.get(1), wcard.get(3));

		System.out.println("I, " + ply.getName() + " accuse " + ac.getPlyerCard().getName()
				+ " of committing the crime in the room " + ac.getRoomCard().getName() + " with "
				+ ac.getWeaponCard().getName());
		if (ply.getTokenPosition() != 2) {
			throw new IllegalPlayException();
		}
		return endGame;

	}

	/**
	 * The method that begin the game
	 */
	@SuppressWarnings("static-access")
	public void setupTheObjects() {

		/**
		 * create six weapon card objects
		 * 
		 * 
		 */

		wepCards[0] = new WeaponCard("Candlestick Card");
		wepCards[1] = new WeaponCard("Knife Card");
		wepCards[2] = new WeaponCard("Lead Pipe Card");
		wepCards[3] = new WeaponCard("Revolver Card");
		wepCards[4] = new WeaponCard("Rope Card");
		wepCards[5] = new WeaponCard("Wrench Card");

		/**
		 * Create 6 player Cards objects
		 * 
		 */
		playerCards[0] = new PlayerCard("Green Card");
		playerCards[1] = new PlayerCard("Mustard Card");
		playerCards[2] = new PlayerCard("Peacock Card");
		playerCards[3] = new PlayerCard("Scarlet Card");
		playerCards[4] = new PlayerCard("White Card");
		playerCards[5] = new PlayerCard("Plum Card");

		/**
		 * Create nine room cards objects
		 */
		room[0] = new RoomCard("Conservatory Card");
		room[1] = new RoomCard("Lounge Card");
		room[2] = new RoomCard("Kitchen Card");
		room[3] = new RoomCard("Library Card");
		room[4] = new RoomCard("Hall Card");
		room[5] = new RoomCard("Study Card");
		room[6] = new RoomCard("Ballroom Card");
		room[7] = new RoomCard("Dining_Room Card");
		room[8] = new RoomCard("Billiard_Room Card");

		WeaponCard wepon = new WeaponCard(wepCards);
		PlayerCard pl = new PlayerCard(playerCards);
		RoomCard rc = new RoomCard(room);

		/**
		 * Shuffle the cards for each individual category
		 */
		wepon.shuffle();
		pl.shuffle();
		rc.shuffle();

		/**
		 * Take the card the first card of each category
		 */
		PlayerCard p = (PlayerCard) pl.getFirstCard();
		RoomCard wp = (RoomCard) rc.getFirstCard();
		WeaponCard ro = (WeaponCard) wepon.getFirstCard();

		/**
		 * a constructor which filled the envelop with the three chosen cards
		 * 
		 * @param p
		 *            p the card chosen from the player cards
		 * @param wp
		 *            wp the card chosen from the room cards
		 * @param ro
		 *            ro the card chosen from the room cards
		 */
		// SecretEnvelope envolop =
		new SecretEnvelope(p, wp, ro);

		/**
		 * set the location of the secret envelope on the board
		 */
		// envolop.setEnvelopeLocation(10, 10);

		/**
		 * Container to hold the remaining cards from the three sets
		 */
		ArrayList<Card> remaindingCards = new ArrayList<Card>();
		/**
		 * get the remaining player cards
		 */
		ArrayList<Card> remain_player_cards = PlayerCard.getPlayerCards();
		System.out.println("remain pl card " + remain_player_cards.size());
		/**
		 * get the remaining suspect cards
		 */
		ArrayList<Card> remain_room_cards = RoomCard.getRoomCards();
		System.out.println("remain rom cards " + remain_room_cards.size());
		/**
		 * get the remaining weapon cards
		 */
		ArrayList<Card> remain_wepon_cards = WeaponCard.getWeaponCards();
		System.out.println("remain wep card " + remain_wepon_cards.size());
		/**
		 * add the remaining player cards to list
		 */
		remaindingCards.addAll(remain_player_cards);
		/**
		 * add the remaining room cards to the list
		 */
		remaindingCards.addAll(remain_room_cards);
		/**
		 * add the remaining weapon cards to the list
		 */
		remaindingCards.addAll(remain_wepon_cards);

		/**
		 * shuffle the three piles of cards together
		 */
		Collections.shuffle(remaindingCards);
		Player[] play = new Player[6];
		play[0] = new Player("Miss Scarlet");
		play[1] = new Player("Coln Mustard");
		play[2] = new Player("Mr Green");
		play[3] = new Player("Mrs. White");
		play[4] = new Player("Mrs. Peacock");
		play[5] = new Player("Prof. Plum");

		Player.setThePlayers(play);
		/**
		 * the position of each players token
		 */
		play[0].setX(2);
		play[0].setY(8);
		play[1].setX(15);
		play[1].setY(0);
		play[2].setX(6);
		play[2].setY(0);
		play[3].setX(0);
		play[3].setY(16);
		play[4].setX(4);
		play[4].setY(21);
		play[5].setX(19);
		play[5].setY(21);

		/**
		 * dealt the shuffled cards to the players. Players are expected to have
		 * an unequal number of cards
		 * 
		 * Assuming the number of players is constand
		 * 
		 */

		play[0].setPlayercard(remaindingCards.subList(0, 3));
		play[1].setPlayercard(remaindingCards.subList(3, 8));
		play[2].setPlayercard(remaindingCards.subList(8, 11));
		play[3].setPlayercard(remaindingCards.subList(11, 14));
		play[4].setPlayercard(remaindingCards.subList(13, 15));
		play[5].setPlayercard(remaindingCards.subList(15, 18));
		/*
		 * 
		 * more classes are coming here
		 */
		Room lounge = new Room("Lounge");
		Room kitchen = new Room("KITCHEN");
		Room ball_room = new Room("BALL ROOM");
		Room hall = new Room("HALL");
		Room study = new Room("STUDY");
		Room library = new Room("LIBRARY");
		Room billia_room = new Room("BILLIARD ROOM");
		Room conservatory = new Room("CONSERVATORY");
		Room dinning_room = new Room("DINING ROOM");
		/**
		 * set the door location of the conservatory room
		 */
		conservatory.setRoomDoor1(3, 17);
		/**
		 * set the first door of the HALL room
		 */
		hall.setRoomDoor2(5, 6);
		hall.setRoomDoor2(6, 8);
		study.setRoomDoor1(0, 3);
		billia_room.setRoomDoor1(0, 0);

		lounge.setRoomDoor1(17, 6);

		//////////////////////////////////////////////////////////

		/**
		 * set the 1st door's location for the LIBRARY
		 */
		final int lib_x = 0, lib_y = 0;
		library.setRoomDoor1(lib_x, lib_y);
		/**
		 * set the 2nd door's location for the LIBRARY
		 */
		final int lib_x2 = 0, lib_y2 = 0;
		library.setRoomDoor2(lib_x2, lib_y2);

		final int kit_x = 0, kit_y = 0;
		kitchen.setRoomDoor1(5, 4);

		final int din_x = 0, din_y = 0;
		dinning_room.setRoomDoor1(14, 6);

		/**
		 * set the 2nd door's location for DINING ROOM
		 */

		final int din_x1 = 0, din_y1 = 0;
		dinning_room.setRoomDoor2(11, 7);

		/**
		 * set the secret door's location for DINING ROOM
		 */
		final int din_sx = 0, din_sy = 0;
		dinning_room.setSecretDoor(din_sx, din_sy);

		/***
		 * set the 1st door location for BALL ROOM
		 */
		final int bal_x = 0, bal_y = 0;
		ball_room.setRoomDoor1(bal_x, bal_y);
		/***
		 * set the 2nd door location for BALL ROOM
		 */
		final int bal_rx = 0, bal_ry = 0;
		ball_room.setRoomDoor2(bal_rx, bal_ry);
		/***
		 * set the 3rd door location for BALL ROOM
		 */
		final int balrom_x = 0, balrom_y = 0;
		ball_room.setRoomDoor3(balrom_x, balrom_y);
		/***
		 * set the 4th door location for BALL ROOM
		 */
		final int balrom_rx = 0, balrom_ry = 0;
		ball_room.setRoomDoor4(balrom_rx, balrom_ry);
		/**
		 * set the secret door's location for ball room
		 */
		final int bal_xx = 0, bal_yy = 0;
		ball_room.setSecretDoor(bal_xx, bal_yy);

	}
}
/**
 * Left to be finished are: conservatory hall billiard and study room
 * 
 * 
 */
