
package edu.buffalo.cse116;

public interface Card {
	void shuffle();
    String getCardName();
	void setFaceUp();
	Card getFirstCard();
	String getName();

}


