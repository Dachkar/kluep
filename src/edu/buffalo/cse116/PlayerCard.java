package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Collections;

public class PlayerCard implements Card {
	private static  ArrayList<Card> player_cards=new ArrayList<Card>();
	private int[][] roomLocation;
	private String name = null;
	
	public PlayerCard(String str) {
		name  = str;
	}

	public PlayerCard(Card[] playerCards) {
		System.out.println("Initial num of player cards " + playerCards.length);
		for (int i = 0; i < playerCards.length; i++) {
			player_cards.add(playerCards[i]);
		}
	
	}

/**
 * shuffle the elements in the collection
 */
	public void shuffle() {
		Collections.shuffle(player_cards);
	}
/**
 * 
 * @return return collection of player cards
 */
	public static  ArrayList<Card> getPlayerCards() {
		return player_cards;
	}
/**
 * 
 * @return return return the removed card in the player cards 
 */
	public Card getFirstCard() {
		return  player_cards.remove(0);
	}
	
@Override
public void setFaceUp() {
for (int i = 0; i < player_cards.size(); i++) {
	
}
}

@Override
public String getCardName() {
	return name;
}

@Override
public String getName() {
	String str[] = name.split("\\s");
	return str[0];
}
}
