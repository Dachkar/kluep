package edu.buffalo.cse116;

/**
 * @author kemok
 *
 */
public class Room {
	private int location[][];
	private String roomName;
	private int[][] roomDoor1;
	private int[][] roomDoor12;
	private int[][] secretDoor;
	private int[][] wall;

	/**
	 * 
	 * @param string
	 *            string parameter representing the names of the rooms
	 */
	public Room(String string) {
		roomName = string;
	}

	public Room() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return return the name of the room
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * 
	 * @return return the location of door one
	 */
	public int[][] getRoomDoor1() {

		return roomDoor1;
	}

	/**
	 * this method create a door for a room and first door for room with more
	 * than 2 doors
	 * 
	 * @param x
	 *            x-coordinate of door's location
	 * @param y
	 *            y-coordinate of door's location
	 */
	public void setRoomDoor1(int x, int y) {

		// this.roomDoor1 = roomDoor;
	}

	/**
	 * this method create the 2nd door for a room with 2 doors
	 * 
	 * @param x
	 *            x-coordinate of the door's location
	 * @param y
	 *            y-coordinate of the door's location
	 */
	public void setRoomDoor2(int x, int y) {

		// this.roomDoor1 = roomDoor;
	}

	/**
	 * this method create the 3rd door for a room with 3 doors
	 * 
	 * @param x
	 *            x-coordinate of the door's location
	 * @param y
	 *            y-coordinate of the door's location
	 */
	public void setRoomDoor3(int x, int y) {
		
		// this.roomDoor1 = roomDoor;
	}

	/**
	 * this method create the 4th door of a room with 4 doors
	 * 
	 * @param x
	 *            x represent the x-coordinate of door's location
	 * @param y
	 *            y represent the y-coordinate of door's location
	 */
	public void setRoomDoor4(int x, int y) {
		// TODO Auto-generated method stub

	}

	/**
	 * this method create the door for the secret exit
	 * 
	 * @param x
	 *            x represent the x-coordinate of door's location
	 * @param y
	 *            x represent the y-coordinate of door's location
	 */
	public void setSecretDoor(int x, int y) {
		// secretDoor = sec;

	}
	
	
	public void setWall(int[][] w){
		
	}
	

	/**
	 * 
	 * @return return the location of the secret exit
	 */
	public int[][] getSecretDoor() {
		return secretDoor;
	}
	
	

}
