package edu.buffalo.cse116;
import java.util.ArrayList;
import java.util.Collections;

public class RoomCard implements Card {

	private static ArrayList<Card> roomCards =  new ArrayList<Card>();
	private String name;
	
	public RoomCard(String str){
		name =str;
	}
	public RoomCard(Card []room) {
		System.out.println("Initial num of room cards " + room.length);
		for (int i = 0; i < room.length; i++) {
		roomCards.add(room[i]);
		}
	}
	public void shuffle() {
		Collections.shuffle(roomCards);
	}
	public static ArrayList<Card> getRoomCards() {
		return roomCards;
	}
	public  Card getFirstCard(){
		return roomCards.remove(0);
	}
	@Override
	public void setFaceUp() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getCardName() {
		// TODO Auto-generated method stub
		return name;
	}
	@Override
	public String getName() {
		String str[] = name.split("\\s");
		return str[0];
	}
	}


