package edu.buffalo.cse116;

import java.util.ArrayList;


/**
 * @author kemok
 *
 */
public class SecretEnvelope {
	private static ArrayList<Card> evelop = new ArrayList<Card>();
	private int x, y;
/**
 * 
 * @param player  player card which show the player who did the murdered
 * @param room room card which show the room the murder occurred
 * @param weapon weapon card which show the weapon that was used in the murder
 **/
	public SecretEnvelope(PlayerCard player, RoomCard room, WeaponCard weapon) {
		evelop.add(player);
		evelop.add(weapon);
		evelop.add(room);
	}
	/**
	 * Sets the location on the envelop
	 * @param x
	 *            represent the x-coordinate of the envelop location
	 * @param y
	 *            represent the y-coordinate envelop location
	 *            
	 */
	public void setEnvelopeLocation(int x, int y) {

	}
	public static ArrayList<Card> getSecretEvelopCards(){
		return evelop;
		
	}
	/**
	 * @return return the location of the Case Confidential File
	 **/

	public int getEnvelopeLocation() {
		return 0;

	}
}
