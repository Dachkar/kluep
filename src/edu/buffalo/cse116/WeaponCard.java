
package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author kemok
 *
 */
public class WeaponCard implements Card {
	private static ArrayList<Card> weaponCard = new ArrayList<Card>();
	private String name;
	public WeaponCard(String string) {
		name = string;
	}
	public WeaponCard(Card[] wepCards) {
		System.out.println("Initial num of wepon cards " + wepCards.length); 
		for (int i = 0; i < wepCards.length; i++) {
			weaponCard.add(wepCards[i]);
		}
	}
	public void shuffle() {
		Collections.shuffle(weaponCard);
	}
	public static ArrayList<Card> getWeaponCards() {
		return weaponCard;
	}
	public  Card getFirstCard(){
		return weaponCard.remove(0);
	}
	@Override
	public void setFaceUp() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getCardName() {
		
		return name;
	}
	@Override
	public String getName() {
		String str[] = name.split("\\s");
		return str[0];
	}
}