package exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class IllegalMoveExcetion extends Exception{
	
public IllegalMoveExcetion(String message){
	super(message);
	//System.out.println("IllegalMoveExcetion.IllegalMoveExcetion()");
    JFrame frame =  new JFrame();
	JOptionPane.showMessageDialog(frame, message);
}
}
