package exceptions;

@SuppressWarnings("serial")
public class IllegalPlayException extends Exception {
public IllegalPlayException(){
	super("Illegal move! Your token must be in the room you mentioned");
}
}
