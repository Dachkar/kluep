package grid;

import java.awt.Graphics;

import javax.swing.JPanel;

public class Grid extends JPanel{
	public static int[][] grid;
	private static int position;

    int col =23;
    int row =24;
	private int green_x=0; //
	private int green_y=180;
	public Grid() {
	
		grid = new int[row][col];
	
		// Row zero//////////////////////////////////////
		grid[0][0] = 0;
		grid[0][1] = 0; // 0=cona room
		grid[0][2] = 0;// 1=hall
		grid[0][3] = 0;// 2=room
		grid[0][4] = 0;//3=door
		grid[0][5] = 0;
		grid[0][6] = 1;
		grid[0][7] = 1;
		grid[0][8] = 1;
		grid[0][9] = 1; /////white token
		grid[0][10] = 2;
		grid[0][11] = 2;
		grid[0][12] = 2;
		grid[0][13] = 1;
		grid[0][14] = 1;
		grid[0][15] = 1;
		grid[0][16] = 1;
		grid[0][17] = 0;
		grid[0][18] = 0;
		grid[0][19] = 0;
		grid[0][20] = 0;
		grid[0][21] = 0;
		grid[0][22] = 0;
		//grid[0][23] = 0;

		//// First row/////////////////////////////////////////
		grid[1][0] = 0;
		grid[1][1] = 0;
		grid[1][2] = 0;
		grid[1][3] = 0;
		grid[1][4] = 0;
		grid[1][5] = 0;
		grid[1][6] = 1;
		grid[1][7] = 1;
		grid[1][8] = 2;
		grid[1][9] = 2;
		grid[1][10] = 2;
		grid[1][11] = 2;
		grid[1][12] = 2;
		grid[1][13] = 2;
		grid[1][14] = 2;
		grid[1][15] = 1;
		grid[1][16] = 1;
		grid[1][17] = 0;
		grid[1][18] = 0;
		grid[1][19] = 0;
		grid[1][20] = 0;
		grid[1][21] = 0;
	    grid[1][22] = 1;
		// grid[1][23] = 1;

		// second row///////////////////////////////////////
		grid[2][0] = 0;
		grid[2][1] = 0;
		grid[2][2] = 0;
		grid[2][3] = 0;
		grid[2][4] = 0;
		grid[2][5] = 0;
		grid[2][6] = 1;
		grid[2][7] = 1;
		grid[2][8] = 2;
		grid[2][9] = 2;
		grid[2][10] = 2;
		grid[2][11] = 2;
		grid[2][12] = 2;
		grid[2][13] = 2;
		grid[2][14] = 2;
		grid[2][15] = 1;
		grid[2][16] = 1;
		grid[2][17] = 0;
		grid[2][18] = 0;
		grid[2][19] = 0;
		grid[2][20] = 0;
		grid[2][21] = 0;
		grid[2][22] = 1;
		// grid[2][23] = 1;

		// third row////////////////////////////////////////////
		grid[3][0] = 0;
		grid[3][1] = 0;
		grid[3][2] = 0;
		grid[3][3] = 0;
		grid[3][4] = 0;
		grid[3][5] = 0;
		grid[3][6] = 1;
		grid[3][7] = 1;
		grid[3][8] = 2;
		grid[3][9] = 2;
		grid[3][10] =2;
		grid[3][11] = 2;
		grid[3][12] = 2;
		grid[3][13] = 2;
		grid[3][14] = 2;
		grid[3][15] = 1;
		grid[3][16] = 1;
		grid[3][17] = 3; //3==door
		grid[3][18] = 0;
		grid[3][19] = 0;
		grid[3][20] = 0;
		grid[3][21] = 0;
		grid[3][22] = 1;
		// grid[3][23] = 1;

		// forth row////////////////////////////////////////
		grid[4][0] = 0;
		grid[4][1] = 0;
		grid[4][2] = 0;
		grid[4][3] = 0;
		grid[4][4] = 0;
		grid[4][5] = 0;
		grid[4][6] = 1;
		grid[4][7] = 1;
		grid[4][8] = 3;
		grid[4][9] = 2;
		grid[4][10]= 2;
		grid[4][11] = 2;
		grid[4][12] = 2;
		grid[4][13] = 2;
		grid[4][14] = 2;
		grid[4][15] = 1;
		grid[4][16] = 1;
		grid[4][17] = 1;
		grid[4][18] = 0;
		grid[4][19] = 0;
		grid[4][20] = 0;
		grid[4][21] = 4; //blue token////////////////////
		grid[4][22] = 1;
		// grid[4][23] = 0;

		//// 5th column//////////////////////////////////////////
		grid[5][0] = 0;
		grid[5][1] = 0;
		grid[5][2] = 0;
		grid[5][3] = 0;
		grid[5][4] = 3;
		grid[5][5] = 0;
		grid[5][6] = 1;
		grid[5][7] = 1;
		grid[5][8] = 2;
		grid[5][9] = 2;
		grid[5][10] =2;
		grid[5][11] =2;
		grid[5][12] =2;
		grid[5][13] =2;
		grid[5][14] =2;
		grid[5][15] =1;
		grid[5][16] =1;
		grid[5][17] =1;
		grid[5][18] =1;//////// 43
		grid[5][19] =1;
		grid[5][20] =1;
		grid[5][21] =1;
		grid[5][22] = 1;
		// grid[5][23] = 1;

		////////////////////////////// 6th row//////////////
		grid[6][0] = 1; ///// green token
		grid[6][1] = 1;
		grid[6][2] = 1;
		grid[6][3] = 1;
		grid[6][4] = 1;
		grid[6][5] = 1;
		grid[6][6] = 1;
		grid[6][7] = 1;
		grid[6][8] = 2;
		grid[6][9] = 2;
		grid[6][10] =3;
		grid[6][11] =3;
		grid[6][12] =3;
		grid[6][13] =2;
		grid[6][14] =2;
		grid[6][15] = 1;
		grid[6][16] = 1;
		grid[6][17] = 1;
		grid[6][18] = 1;
		grid[6][19] = 1;
		grid[6][20] = 1;
		grid[6][21] = 1;
		grid[6][22]= 0;
		// grid[6][23] = 0;

		//// 7th row /////////////////////////
		grid[7][0] = 1;
		grid[7][1] = 1;
		grid[7][2] = 1;
		grid[7][3] = 1;
		grid[7][4] = 1;
		grid[7][5] = 1;
		grid[7][6] = 1;
		grid[7][7] = 1;
		grid[7][8] = 1;
		grid[7][9] = 1;
		grid[7][10] = 1;
		grid[7][11] = 1;
		grid[7][12] = 1;
		grid[7][13] = 1;
		grid[7][14] = 1;
		grid[7][15] = 1;
		grid[7][16] = 1;
		grid[7][17] = 2;
		grid[7][18] = 2;//////// 43
		grid[7][19] = 2;
		grid[7][20] = 2;
		grid[7][21] = 2;
		grid[7][22] = 1;
		//grid[7][23] = 8;

		// 8th row/////////////////////////////////////
		grid[8][0] = 2;
		grid[8][1] = 2;
		grid[8][2] = 2;
		grid[8][3] = 2;
		grid[8][4] = 2;
		grid[8][5] = 1;
		grid[8][6] = 1;
		grid[8][7] = 1;
		grid[8][8] = 1;
		grid[8][9] = 1;
		grid[8][10] = 1;
		grid[8][11] = 1;
		grid[8][12] = 1;
		grid[8][13] = 1;
		grid[8][14] = 1;
		grid[8][15] = 1;
		grid[8][16] = 1;
		grid[8][17] = 3;
		grid[8][18] = 2;
		grid[8][19] = 2;
		grid[8][20] = 2;
		grid[8][21] = 0;
		grid[8][22]= 0;
		//grid[8][23] = 0;

		// 9th row///////////////////////////////////////////////
		grid[9][0] = 2;
		grid[9][1] = 2;
		grid[9][2] = 2;
		grid[9][3] = 2;
		grid[9][4] = 2;
		grid[9][5] = 2;
		grid[9][6] = 2;
		grid[9][7] = 2;
		grid[9][8] = 1;
		grid[9][9] = 1;
		grid[9][10] =8; // 8==seret evenvelop
		grid[9][11] =8;
		grid[9][12] =8;
		grid[9][13] =8;
		grid[9][14] =8;
		grid[9][15] =1;
		grid[9][16] =1;
		grid[9][17] =2;
		grid[9][18] =2;
		grid[9][19] =2;
		grid[9][20] =2;
		grid[9][21] =2;
		grid[9][22] = 1;
		// grid[9][23] = 1;

		// 10th row///////////////////////////////////////////
		grid[10][0] = 2;
		grid[10][1] = 2;
		grid[10][2] = 2;
		grid[10][3] = 2;
		grid[10][4] = 2;
		grid[10][5] = 2;
		grid[10][6] = 2;
		grid[10][7] = 2;
		grid[10][8] = 1;
		grid[10][9] = 1;
		grid[10][10] =8;
		grid[10][11] =8;
		grid[10][12] =8;
		grid[10][13] =8;
		grid[10][14] =8;
		grid[10][15] =1;/////////////
		grid[10][16] =1;
		grid[10][17] =2;
		grid[10][18] =2;
		grid[10][19] =2;
		grid[10][20] =2;
		grid[10][21] =2;
		grid[10][22] = 0;
		// grid[10][23] = 0;

		//// 11th column///////////////////////////////////////////
		grid[11][0] = 2;
		grid[11][1] = 2;
		grid[11][2] = 2;////////////
		grid[11][3] = 2;
		grid[11][4] = 2;
		grid[11][5] = 2;
		grid[11][6] = 2;
		grid[11][7] = 3;
		grid[11][8] = 1;
		grid[11][9] = 1;
		grid[11][10] =8;
		grid[11][11] =8;
		grid[11][12] = 8;
		grid[11][13] = 8;
		grid[11][14] = 8;
		grid[11][15] = 1;
		grid[11][16] = 1;
		grid[11][17] = 2;
		grid[11][18] = 2;
		grid[11][19] = 2;
		grid[11][20] = 2;
		grid[11][21] = 2;
		grid[11][22]= 1;
		// grid[11][23] = 1;

		// 12th row///////////////////////////////////////////////

		grid[12][0] = 2;
		grid[12][1] = 2;
		grid[12][2] = 2;
		grid[12][3] = 2;
		grid[12][4] = 2;
		grid[12][5] = 2;
		grid[12][6] = 2;
		grid[12][7] = 2;
		grid[12][8] = 1;
		grid[12][9] = 1;
		grid[12][10] = 8;
		grid[12][11] = 8;
		grid[12][12] = 8;
		grid[12][13] = 8;
		grid[12][14] = 8;
		grid[12][15] = 1;
		grid[12][16] = 1;
		grid[12][17] = 1;
		grid[12][18] = 1;
		grid[12][19] = 1;
		grid[12][20] = 1;
		grid[12][21] = 1;
		grid[12][22] = 0;
		// grid[12][23] = 0;

		//// 13th column//////////////////////////////////////////
		grid[13][0] = 2;
		grid[13][1] = 2;
		grid[13][2] = 2;
		grid[13][3] = 2;
		grid[13][4] = 2;
		grid[13][5] = 2;
		grid[13][6] = 2;
		grid[13][7] = 2;
		grid[13][8] = 1;
		grid[13][9] = 1;
		grid[13][10] =8;
		grid[13][11] =8;
		grid[13][12] =8;
		grid[13][13] =8;
		grid[13][14] =8;
		grid[13][15] =1;
		grid[13][16] =1;
		grid[13][17] =2;
		grid[13][18] =2;
		grid[13][19] =3; ///////doorr
		grid[13][20] =2;
		grid[13][21] =2;
	    grid[13][22] = 1;
		

		// 14th row////////////////////////////////////////
		grid[14][0] = 2;
		grid[14][1] = 2;
		grid[14][2] = 2;
		grid[14][3] = 2;
		grid[14][4] = 3; // doorrr
		grid[14][5] = 2;
		grid[14][6] = 2;
		grid[14][7] = 2;
		grid[14][8] = 1;
		grid[14][9] = 1;
		grid[14][10] =8;
		grid[14][11] =8;
		grid[14][12] = 8;
		grid[14][13] = 8;
		grid[14][14] = 8;
		grid[14][15] = 1;
		grid[14][16] = 2;
		grid[14][17] = 2;
		grid[14][18] = 2;
		grid[14][19] = 2;
		grid[14][20] = 2;
		grid[14][21] = 2;
		grid[14][22] = 0;
		// grid[14][23] = 0;

		//// 15th column//////////////////////////////////////////////
		grid[15][0] = 1;  //////////yellow token
		grid[15][1] = 1;
		grid[15][2] = 1;
		grid[15][3] = 1;
		grid[15][4] = 1;
		grid[15][5] = 1;
		grid[15][6] = 1;
		grid[15][7] = 1;
		grid[15][8] = 1;
		grid[15][9] = 1;
		grid[15][10] = 8;
		grid[15][11] = 8;
		grid[15][12] = 8;
		grid[15][13] = 8;
		grid[15][14] = 8;
		grid[15][15] = 1;
		grid[15][16] = 3; //3-2
		grid[15][17] = 2;
		grid[15][18] = 2;
		grid[15][19] = 2;
		grid[15][20] = 2;
		grid[15][21] = 2;
		grid[15][22]= 1;
		// grid[15][23] = 1;

		// 16th row//////////////////////////////////////////////
		grid[16][0] = 1;
		grid[16][1] = 1;
		grid[16][2] = 1;
		grid[16][3] = 1;
		grid[16][4] = 1;
		grid[16][5] = 1;
		grid[16][6] = 1;
		grid[16][7] = 1;
		grid[16][8] = 1;
		grid[16][9] = 1;
		grid[16][10] =1;
		grid[16][11] =1;
		grid[16][12] =1;
		grid[16][13] =1;
		grid[16][14] =1;
		grid[16][15] =1;
		grid[16][16] =2; ///2-1
		grid[16][17] =2;
		grid[16][18] =2;
		grid[16][19] =2;
		grid[16][20] =2;
		grid[16][21] =2;
		grid[16][22] = 0;
		// grid[16][23] = 0;

		//// 17th column/////////////////////////////////////////////////
		grid[17][0] = 1;
		grid[17][1] = 1;
		grid[17][2] = 1;
		grid[17][3] = 1;
		grid[17][4] = 1;
		grid[17][5] = 1;
		grid[17][6] = 1;
		grid[17][7] = 1;
		grid[17][8] = 1;
		grid[17][9] = 2;
		grid[17][10] = 2;
		grid[17][11] = 2;
		grid[17][12] = 2;
		grid[17][13] = 2;
		grid[17][14] = 2;
		grid[17][15] = 1;
		grid[17][16] = 1;
		grid[17][17] = 2;
		grid[17][18] = 2;
		grid[17][19] = 2;
		grid[17][20] = 2;
		grid[17][21] = 2;
		grid[17][22] = 1;
		// grid[17][23] = 1;

		// 18th roww//////////////////////////////////////////////
		grid[18][0] = 0;
		grid[18][1] = 0;
		grid[18][2] = 0;
		grid[18][3] = 0;
		grid[18][4] = 0;
		grid[18][5] = 0;
		grid[18][6] = 3; //doorrrrrrrrrrrrrrr
		grid[18][7] = 1;
		grid[18][8] = 1;
		grid[18][9] = 2;
		grid[18][10] =2;
		grid[18][11] =2;
		grid[18][12] =2;
		grid[18][13] =2;
		grid[18][14] =2;
		grid[18][15] =1;
		grid[18][16] =1;
		grid[18][17] =1;
		grid[18][18] =1;
		grid[18][19] =1;
		grid[18][20] =1;
		grid[18][21] =1;
		grid[18][22] = 0;
		// grid[18][23] = 0;

		//// 19th column//////////////////////////////////
		grid[19][0] = 0;
		grid[19][1] = 0;
		grid[19][2] = 0;
		grid[19][3] = 0;
		grid[19][4] = 0;
		grid[19][5] = 0;
		grid[19][6] = 0;
		grid[19][7] = 1;
		grid[19][8] = 1;
		grid[19][9] = 2;
		grid[19][10] =2;
		grid[19][11] =2;
		grid[19][12] =2;
		grid[19][13] =2;
		grid[19][14] =2;
		grid[19][15] =1;
		grid[19][16] =1;
		grid[19][17] =1;
		grid[19][18] =1;
		grid[19][19] =1;
		grid[19][20] =1;
		grid[19][21] =4;     /////////purple torken
		grid[19][22] = 1;
		// grid[19][23] = 1;

		// 20th roow//////////////////////////////////
		grid[20][0] = 0;
		grid[20][1] = 0;
		grid[20][2] = 0;
		grid[20][3] = 0;
		grid[20][4] = 0;
		grid[20][5] = 0;
		grid[20][6] = 0;
		grid[20][7] = 1;
		grid[20][8] = 1;
		grid[20][9] = 2;
		grid[20][10] =2;
		grid[20][11] =2;
		grid[20][12] =2;
		grid[20][13] =2;
		grid[20][14] =3; //////doorrrr
		grid[20][15] =1;
		grid[20][16] =0;
		grid[20][17] =3;/////////dooor
		grid[20][18] =0;
		grid[20][19] =0;
		grid[20][20] =0;
		grid[20][21] =0;
		grid[20][22] = 0;
		// grid[20][23] = 0;

		//// 21th row////////////////////////////////////
		grid[21][0] =0;
		grid[21][1] =0;
		grid[21][2] =0;
		grid[21][3] =0;
		grid[21][4] =0;
		grid[21][5] =0;
		grid[21][6] =0;
		grid[21][7] =1;
		grid[21][8] =1;
		grid[21][9] =2;
		grid[21][10] =2;
		grid[21][11] =2;
		grid[21][12] =2;
		grid[21][13] =2;
		grid[21][14] =2;
		grid[21][15] =1;
		grid[21][16] =0;
		grid[21][17] =0;
		grid[21][18] =0;
		grid[21][19] =0;
		grid[21][20] =0;
		grid[21][21] =0;
		grid[21][22] = 1;
		// grid[21][23] = 1;

		// 22nd row///////////////////////////////////////////////////////
		grid[22][0] = 0;
		grid[22][1] = 0;
		grid[22][2] = 0;
		grid[22][3] = 0;
		grid[22][4] = 0;
		grid[22][5] = 0;
		grid[22][6] = 0;
		grid[22][7] = 1;
		grid[22][8] = 4; /// red torken
		grid[22][9] = 2;
		grid[22][10] = 2;
		grid[22][11] = 2;
		grid[22][12] = 2;
		grid[22][13] = 2;
		grid[22][14] = 2;
		grid[22][15] = 1;
		grid[22][16] = 0;
		grid[22][17] = 0;
		grid[22][18] = 0;
		grid[22][19] = 0;
		grid[22][20] = 0;
		grid[22][21] = 0;
		grid[22][22] = 1;
		
/////////////////////////////////////
		
		grid[23][0] = 0;
		grid[23][1] = 0;
		grid[23][2] = 0;
		grid[23][3] = 0;
		grid[23][4] = 0;
		grid[23][5] = 0;
		grid[23][6] = 0;
		grid[23][7] = 1;
		grid[23][8] = 1; /// red torken
		grid[23][9] = 2;
		grid[23][10] = 2;
		grid[23][11] = 2;
		grid[23][12] = 2;
		grid[23][13] = 2;
		grid[23][14] = 2;
		grid[23][15] = 1;
		grid[23][16] = 0;
		grid[23][17] = 0;
		grid[23][18] = 0;
		grid[23][19] = 0;
		grid[23][20] = 0;
		grid[23][21] = 0;
		grid[23][21] = 0;
	
	/*	System.out.println("0=corner rooms" + "\n1=hallways" + "\n2=room(not including the corner rooms" + "\n3=doors"
				+ "\n4=token starting position" + "\n8= secret envelop location");
		for (int i = 0; i < grid.length; i++) {
			System.out.println();
			for (int j = 0; j < grid[i].length; j++) {
				System.out.print(grid[i][j] + "  ");
			}
		}
		System.out.println();*/
		
	}

	public static int getPlayerPosi() {

		return position;
	}
	public static int getGrid(int x, int y){
		return grid[x][y];
	}

	public static void setPlayerPo(int x, int y) {
		position = grid[x][y];

	}
	@Override
	public void paint(Graphics g) {
	//g.setColor(Color.green);
		
	g.fillOval(green_x, green_y, 30, 30);
	
	}


}
