package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import grid.Grid;
import gui1.Gui2;
import players.Player;

public class BoardGui extends JPanel implements KeyListener,ActionListener {
	/**
	 * Starting position of Mr Green
	 */
	Grid gridm;
	int x_green = 180;
	int y_green = 0;
	/**
	 * Starting position of Coln Mustard
	 */
	int x_mustard = 450;
	int y_mustard = 0;
	/**
	 * Starting position of Miss Scarlet
	 */
	int x_scarlet = 690;
	int y_scarlet = 240;
	/**
	 * Starting position of Prof. Peacock
	 */
	int x_plum = 120;
	int y_plum = 630;
	/**
	 * Starting position of Mrs. Peacock
	 */
	int x_peacock = 568;
	int y_peacock = 630;
	/**
	 * Starting position of Mrs. White
	 */
	int x_mrsWhite = 0;
	int y_mrsWhite = 480;

	/**
	 * Hold the player the index of player
	 */

	ArrayList<Player> player;
	private JButton leftB;
	private JButton leftR;
	private JButton leftU;
	private JButton leftD;

	public BoardGui() {
		JPanel p =  new JPanel();
		setLayout(new BorderLayout());
		p.setLayout(new BorderLayout(2,2));
		leftB =  new JButton("go left");
		leftB.addActionListener(this);
		leftR =  new JButton("go right");
		leftR.addActionListener(this);

		leftU =  new JButton("go up");
		leftU.addActionListener(this);
		
		leftD =  new JButton("go down");
		leftD.addActionListener(this);
		
		p.add(leftB, BorderLayout.SOUTH);
		p.add(leftR, BorderLayout.NORTH);
		p.add(leftU, BorderLayout.EAST);
		p.add(leftD, BorderLayout.WEST);
		add(p, BorderLayout.WEST);
		
		
		gridm = new Grid();
		player = Player.getThePlayers();
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(true);

	}

	public void paint(Graphics g) {

		super.paint(g);

		int x_axis = 0, y_axis = 0;
		int size = 30;

		for (int y = 0; y < 22; y++) {
			for (int x = 0; x < 24; x++) {
				if (Grid.getGrid(x, y) == 2 || Grid.getGrid(x, y) == 0) {
					g.setColor(Color.blue);
					g.fillRect(x_axis, y_axis, 30, 30);
				}
				if (Grid.getGrid(x, y) == 3) {
					g.setColor(Color.MAGENTA);
					g.fillRect(x_axis, y_axis, 30, 30);
				}
				if (Grid.getGrid(x, y) == 3) {
					g.setColor(Color.ORANGE);
					g.fillRect(x_axis, y_axis, 30, 30);
				}
				if (Grid.getGrid(x, y) == 8) {
					g.setColor(Color.lightGray);
					g.fillRect(x_axis, y_axis, 30, 30);
				}
				if (Grid.getGrid(x, y) == 1) {
					Color c = new Color(129, 129, 129);
					g.setColor(c);
					g.drawRect(x_axis, y_axis, 30, 30);

				}
				x_axis += size;
			}
			x_axis = 0;
			y_axis += size;

		}

		// mr green;
		g.setColor(Color.green);
		g.fillOval(x_green, y_green, 30, 30);

		// mustard
		g.setColor(Color.yellow);
		g.fillOval(x_mustard, y_mustard, 30, 30);

		// Scarlet
		g.setColor(Color.RED);
		g.fillOval(x_scarlet, y_scarlet, 30, 30);

		// plum
		Color purple = new Color(0, 128, 0);
		g.setColor(purple);
		g.fillOval(x_plum, y_plum, 30, 30);

		// peacock
		g.setColor(Color.blue);
		g.fillOval(x_peacock, y_peacock, 30, 30);

		// Mrs White
		g.setColor(Color.lightGray);
		g.fillOval(x_mrsWhite, y_mrsWhite, 30, 30);

	}

	@SuppressWarnings("static-access")
	@Override
	public void keyPressed(KeyEvent evt) {

		int jum = 30;
		Gui2 g = new Gui2();
		int index = 3;//g.playIsOn();
		int x = player.get(index).getX();
		int y = player.get(index).getY();
		if (gridm.getGrid(x, y) != 1) {
			jum -= jum;
		}

		switch (player.get(index).getName()) {

		/// Mr Green
		case "Mr Green":
			switch (evt.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				x_green -= jum;
				x = x + 1;
				this.repaint();

				break;
			case KeyEvent.VK_RIGHT:
				x_green += jum;
				x = x + 1;
				this.repaint();
				break;
			case KeyEvent.VK_UP:
				y_green -= jum;
				y = y + 1;
				this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				y_green += jum;
				y = y + 1;
				this.repaint();
				break;
			}
			break;
		/////////////////////////////////////////////
		case "Prof. Plum":
			switch (evt.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				x_plum -= jum;
				x = x + 1;
				;
				this.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				x_plum += jum;
				x = x + 1;
				;
				this.repaint();
				break;
			case KeyEvent.VK_UP:
				y = y + 1;
				y_plum -= jum;
				this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				y = y + 1;
				y_plum += jum;
				this.repaint();
				break;
			}
			break;
		////////////////////////////
		case "Mrs. White":
			switch (evt.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				x_mrsWhite -= jum;
				x = x + 1;
				this.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				x_mrsWhite += jum;
				x = x + 1;
				this.repaint();
				break;
			case KeyEvent.VK_UP:
				y_mrsWhite -= jum;
				y = y + 1;
				this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				y = y + 1;
				y_mrsWhite += jum;
				this.repaint();
				break;
			}
			break;
		///////////////////////////////////////////
		case "Miss Scarlet":
			switch (evt.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				x_scarlet -= jum;
				x++;
				this.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				x_scarlet += jum;
				x++;
				this.repaint();
				break;
			case KeyEvent.VK_UP:
				y_scarlet -= jum;
				y++;
				this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				y_scarlet += jum;
				y++;
				this.repaint();
				break;
			}
			break;
		////////////////////////////////
		case "Coln Mustard":
			switch (evt.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				x_mustard -= jum;
				x++;
				this.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				x_mustard += jum;
				x++;
				this.repaint();
				break;
			case KeyEvent.VK_UP:
				y_mustard -= jum;
				y++;
				this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				y_mustard += jum;
				y++;
				this.repaint();
				break;
			}
			break;

		case "Mrs. Peacock":
			switch (evt.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				x_peacock -= jum;
				x++;
				this.repaint();
				break;
			case KeyEvent.VK_RIGHT:
				x_peacock += jum;
				x++;
				this.repaint();
				break;
			case KeyEvent.VK_UP:
				y_peacock -= jum;
				y++;
				this.repaint();
				break;
			case KeyEvent.VK_DOWN:
				y_peacock += jum;
				y++;
				this.repaint();
				break;
			}
			break;
		default:
			break;
		}
		player.get(index).setX(x);
		player.get(index).setY(y);

	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==leftU){
			y_peacock -= 30;
			this.repaint();
		}
		
	}
}