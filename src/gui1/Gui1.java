package gui1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import edu.buffalo.cse116.Board;
import exceptions.IllegalMoveExcetion;
import exceptions.IllegalPlayException;
import gui.BoardGui;

public class Gui1 extends JFrame {

	public Gui1() {

		GridBagConstraints c = new GridBagConstraints();
		
		setLayout(new GridBagLayout());

		JPanel panel = new JPanel( new BorderLayout());
	
		JPanel board = new JPanel(new BorderLayout());
	
        board.setSize(new Dimension(20,20));

		BoardGui b = new BoardGui();
		Gui2 arrows = new Gui2();

		panel.add(arrows);
		
		board.add(panel, BorderLayout.EAST);
		c.ipadx =800;
		c.ipady=800;
		c.gridx=0;
		c.gridy=0;
		c.gridheight=200;
		c.gridwidth=200;
		add(b,c);
		
		c.gridx=10;
		c.gridy=10;
		
		add(board,c);

	}

	public static void main(String[] args) {
		
				Board br;
				try {
					br = new Board();
					br.setupTheObjects();
				} catch (IllegalMoveExcetion | IllegalPlayException e) {
					e.printStackTrace();
				}
				Gui1 b = new Gui1();
				b.setSize(1000, 750);
				b.setVisible(true);
				b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		

	}

	
}
