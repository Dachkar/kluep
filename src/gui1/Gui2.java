package gui1;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import edu.buffalo.cse116.Card;
import gui.BoardGui;
import players.Die;
import players.Player;

public class Gui2 extends JPanel implements ActionListener, KeyListener {
	
	JTextArea text;
	JLabel jLabel;
	private JButton dice;
	private Button show;

	private BoardGui board;
	private ArrayList<Player> turn;
	int next;
	JPanel jp1, jp2;

	private JButton start;
	private int steps=0;
	private JTextArea tearea;
	private JPanel jp3;
	private JPanel jp4;
	private JPanel jp5;
	private JPanel p;
	private JButton fini;

	public Gui2() {
		turn = Player.getThePlayers();
		board = new BoardGui();
		jp1 = new JPanel();
		jp1.setLayout(new GridLayout(5,0));
		jp2 =  new JPanel();
		jp3 =  new JPanel();
		jp4 = new JPanel();
		jp5 = new JPanel();
		p = new JPanel();
	

		addKeyListener(this);
		createButton();
	}

	public int playIsOn() {
				next = (next + turn.size()) % 1;
		return next;
		
	}

	@SuppressWarnings("static-access")
	private void createButton() {

		fini =  new JButton();
		fini.setText("Finished Turn");
		fini.addActionListener(this);
		tearea = new JTextArea();
		text = new JTextArea();
		
		text.setEditable(false);
		text.setLineWrap(true);

		text.setText(turn.get(next).getName() + " it is your turn to play");

		text.setFont(new Font("Arial", Font.LAYOUT_LEFT_TO_RIGHT, 15));
		dice = new JButton("dice");
		dice.addActionListener(this);

		show = new Button("Show Card");
		show.addActionListener(this);
		start = new JButton("start");
		start.addActionListener(this);

		jp5.add(show);
		jp3.add(dice);
		jp4.add(start);
		p.add(text);
		
	    
		//jp1.add(text, BorderLayout.SOUTH);
		jp1.add(jp3, BorderLayout.WEST);
		jp1.add(jp4, BorderLayout.NORTH);
		jp1.add(jp5, BorderLayout.EAST);

		
	    add(p);
		add(jp1);
		//add(jp2);
		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==start){
			text.setText("Press roll die");
		
		}

		if (e.getSource() == dice) {
			turn.get(next).setRollDie();
			steps =turn.get(next).getRoll();
			text.setText(Integer.toString(steps));
		}
		if (e.getSource() == show) {
			
			List<Card> cards = turn.get(next).getPlayerCard();
			String str = "";
			for (int i = 0; i < cards.size(); i++) {
				str += cards.get(i).getCardName() + "\n";
			}
			String name = turn.get(next).getName();
			JOptionPane.showMessageDialog(null, name + " cards: \n "+ str);
		}
		if(e.getSource() ==fini){
			playIsOn();
		}
	
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
