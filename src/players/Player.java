package players;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import edu.buffalo.cse116.Card;
import exceptions.IllegalMoveExcetion;
import grid.Grid;

public class Player {
	private static List<Card> playercard = new ArrayList<Card>();
	private static ArrayList<Player> players = new ArrayList<Player>();
	private String name;
	private  int x;
	private  int y;
	JRadioButton action[];

	public boolean setAccusation = false;
	private int posi;
	private int roll;

	// playercard = new ArrayList<Card>();
	// players = new ArrayList<Player>()

	/**
	 * 
	 * @param plName
	 *            plName name of player passed in to a constructor as a
	 *            parameter of a string type
	 */
	public Player(String plName) {
		name = plName;
	}

	/**
	 * 
	 * @return returns return a list of cards a particular player has
	 */
	public static List<Card> getPlayercards() {
		return playercard;
	}

	/**
	 * 
	 * @param list
	 *            remaindingCards represent the dealt cards assign to a player
	 */
	public void setPlayercard(List<Card> list) {
		playercard = list;
	}

	/**
	 * 
	 * @return return return the name of a player
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            name given to a player
	 */
	public void setName(String name) {
		name = name;
	}

	public void setRollDie() {
		Die rollDie = new Die();
		roll=rollDie.playDie();
	}

	public static void setThePlayers(Player[] play) {
		for (int i = 0; i < play.length; i++) {
			players.add(play[i]);
		}

	}

	public static ArrayList<Player> getThePlayers() {
		/*
		 * for (int i = 0; i < players.size(); i++) {
		 * System.out.println(players.get(i).getName()); }
		 */
		return players;

	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setTokenPosition(int xx, int yy) {
		/*
		 * x=xx; y=yy;
		 */
		Grid.setPlayerPo(xx, yy);

	}

	public List<Card> getPlayerCard() {

		return playercard;
	}

	public int getTokenPosition() {
		return 0;// posi =Grid.getPlayerPosi();
	}

	public boolean makeAccusation(Player p) {
		Scanner in = new Scanner(System.in);
		String answer = "";
		System.out.println(p.getName() + " do you want to make an accusation? Y/N");
		answer = in.nextLine();
		if (Character.toUpperCase(answer.charAt(0)) == 'Y') {
			return true;
		}
		return false;
	}

	public boolean useSecretPassage(Player ply) {

		Scanner in = new Scanner(System.in);
		String answer = "";
		System.out.println(ply.getName() + " do you want to make an accusation? Y/N");
		answer = in.nextLine();
		if (Character.toUpperCase(answer.charAt(0)) == 'Y') {
			if (ply.getTokenPosition() != 0) {
				try {
					throw new IllegalMoveExcetion(
							ply.getName() + "your token most be in the corner room to make that move");
				} catch (IllegalMoveExcetion e) {
					e.printStackTrace();
				}
			}
			// return true;
		}
		// return false
		return true;
	}

	public  boolean isMoveLegal(int next_move, Player ply) throws IllegalMoveExcetion {
		int curenX = ply.getX();
		int curenY = ply.getY();
		int xDiff = Math.abs(curenX - (ply.getX()));
		int yDiff = Math.abs(curenY - (ply.getY()));
		//int die = roll;
		/*
		 * for (int i = 1; i <= next_move; i++) { int xDiff = Math.abs(curenX -
		 * (ply.getX() + i)); int yDiff = Math.abs(curenY - (ply.getY() + i));
		 * ply.setTokenPosition((ply.getX() + i), (ply.getY() + i)); int die;
		 */
		if (xDiff + yDiff != roll) {

			throw new IllegalMoveExcetion(ply.getName() + "you cannot move diagonally!");
		}

		return true;

		/*
		 * int rows = ply.getX(); int cols = ply.getY();
		 * 
		 * if(ply.getTokenPosition()==1){ while(rol1>0){
		 * if((rows==ply.getX())&&(cols==cols+1)){ //if(){ rol1--; return true;
		 * } else{ return false; } } if((rows==ply.getX())&&(cols==cols-1)){
		 * rol1--; return true; } if((rows==rows+1)&&(cols==ply.getY())){
		 * 
		 * rol1--; return true; } if((rows==rows-1)&&(cols==ply.getY())){
		 * rol1--; return true; } else{ throw new
		 * IllegalMoveExcetion(ply.getName()+"You made an illegal move!"); } }
		 */

	}

	public void setAccusation(boolean setTrue) {
		setAccusation = setTrue;

	}

	public boolean getAccusation() {
		return setAccusation;
	}

	public boolean takeAction() {
		JFrame frame = new JFrame("Option");
		ButtonGroup bg =  new ButtonGroup();
		JPanel jpan = new JPanel();
		jpan.setLayout(new GridLayout(3,1));
		
		 JPopupMenu popupMenu = new JPopupMenu("Title");

		    // Cut
		    JMenuItem cutMenuItem = new JMenuItem("Accusation");
		    popupMenu.add(cutMenuItem);

		    // Copy
		    JMenuItem copyMenuItem = new JMenuItem("Suggestion");
		    popupMenu.add(copyMenuItem);
		    // Paste
		    JMenuItem pasteMenuItem = new JMenuItem("Seret Passage");
		    pasteMenuItem.setEnabled(false);
		    popupMenu.add(pasteMenuItem);

		    // Separator
		    popupMenu.addSeparator();

		    JMenuItem findMenuItem = new JMenuItem("Find");
		    popupMenu.add(findMenuItem);
		    JButton label = new JButton();
		    frame.add(label);
		    label.setComponentPopupMenu(popupMenu);
		    
		
		/*action = new JRadioButton[4];
		for (int i = 0; i < action.length; i++) {
			action[i] = new JRadioButton();
			
		}
		//bg.add(action[0]);
		action[0].setText("Suggestion");
		
		bg.add(action[0]);
		jpan.add(action[0]);
		action[2].setText("Play");
		bg.add(action[1]);
		jpan.add(action[1]);
		action[1].setText("Accusation");
		bg.add(action[2]);
		jpan.add(action[2]);
		
		frame.add(jpan);
		
		action[0].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		action[1].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		action[1].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		
*/
		    frame.setSize(320,320);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		return true;
	}

	public int getRoll() {
		
		return roll;
	}

	public void setX(int x) {
		this.x=x;
		
	}

	public void setY(int y) {
		
		this.y=y;
	}

}
