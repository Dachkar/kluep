package ui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class IllegalInputException extends Exception {

	public IllegalInputException(String string) {
		super(string);
	    JFrame frame =  new JFrame();
		JOptionPane.showMessageDialog(frame, string);
	}

}
