package ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
//import javax.swing.text.StyledEditorKit.ForegroundAction;

public class SuggestionPane extends JTextField {
	
	UI _ui;
	int _state = 0;
	ArrayList<String> _inputs = new ArrayList<String>();
	private ArrayList<String> _room = new ArrayList<String>();
	private ArrayList<String> _player = new ArrayList<String>();
	private ArrayList<String> _weapon = new ArrayList<String>();
	private String room[]={"Study", "Hall", "Lounge", "Dining Room", "Kitchen", "Ballroom", "Conservatory", "Billiard Room", "Library"};
	private String player[] ={"Ms. Scarlet","Col. Mustard","Mrs. White","Rev. Green","Mrs. Peacock","Prof. Plum"};
	private String weapon[] = {"Candlestick","Poison","Rope","Gloves","Horseshoe","Knife","Lead Pipe","RevolverWrench"};
	/**
	 * Necessary because inside an anon. inner class, 'this' will not refer to the SuggestionPane.
	 */
	SuggestionPane _self = this;
	
	public SuggestionPane(UI ui) {
		for (int i = 0; i < room.length; i++) {
			_room.add(room[i]);
		}
		for (int i = 0; i < player.length; i++) {
			_player.add(player[i]);
		}
		for (int i = 0; i < weapon.length; i++) {
			_weapon.add(weapon[i]);
		}
		
		_ui = ui;
		this.setEnabled(false);
		
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String input = e.getActionCommand();
				
				switch (_state) {
				
					case 0:
						_state++;
						_self.setText("");
						
						_inputs.add(input);
						_ui.displayInstruction("Please enter the room.");
						
						if(!_weapon.contains(input)){
							try {
								throw new IllegalInputException("Invalid Input!");
							} catch (IllegalInputException e1) {
								e1.printStackTrace();
							}
						}
						break;
						
					case 1:
						if(!_room.contains(input)){
							try {
								throw new IllegalInputException("Invalid Input!");
							} catch (IllegalInputException e1) {
								e1.printStackTrace();
							}
						}
						_state++;
						_self.setText("");
						_inputs.add(input);
						_ui.displayInstruction("Please enter the player.");
						break;
						
					case 2:
						if(!_player.contains(input)){
							try {
								throw new IllegalInputException("Invalid Input!");
							} catch (IllegalInputException e1) {
								e1.printStackTrace();
							}
						}
						_state = 0;
						_self.setText("");
						_inputs.add(input);
						///ui.makeSuggestion();
						_self.setEnabled(false);
						
						// submit the suggestion
						_ui.passSuggestion(_inputs);
					
						break;
				
				}
				
			}
		});
	}
	
	public void grabSuggestionInput() {
		_state = 0;
		_inputs = new ArrayList<String>();
		 this.setEnabled(true);
		_ui.displayInstruction("Please enter the weapon.");
	}

	public void grabAccusationInput() {
		_state = 0;
		_inputs = new ArrayList<String>();
		 this.setEnabled(true);
		_ui.displayInstruction("Enter the weapon involved.");
		
	}
}
