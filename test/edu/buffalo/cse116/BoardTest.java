package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import exceptions.IllegalMoveExcetion;
import exceptions.IllegalPlayException;
import players.Player;

public class BoardTest {

	Board b;
	@Before
	public void firstTest() throws IllegalMoveExcetion, IllegalPlayException {
		b=new Board();
		  b.setupTheObjects();
	
	}
	@Test
	public void testStartTheMove() throws IllegalMoveExcetion, IllegalPlayException {
		ArrayList<Player> ar = Player.getThePlayers();
		String s[] = new String[ar.size()];
		for (int i = 0; i < s.length; i++) {
			s[i] =  ar.get(i).getName();
					
		}
		 b.startTheMove(ar.get(0));
			String i =  s[2];
			System.out.println(i);
			assertTrue(i.equals("Col"));
	}
	@Test
	public void testSugestion() {
		ArrayList<Player> ar = Player.getThePlayers();
		assertTrue((ar.get(1).getTokenPosition()==0) ||(ar.get(1).getTokenPosition()==2));
	}

}
