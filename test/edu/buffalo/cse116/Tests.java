package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import exceptions.IllegalMoveExcetion;
import exceptions.IllegalPlayException;
import players.Player;

public class Tests {
	
	/*ALL THE TESTS ARE INCORPORATED IN THE CODE. cHECK PLAYER CLASS FOR MOVE LEGALITY, 
	 * FOR HORIZONTAL, VERTICAL AND DIAGONAL MOVES.*/
	
	Board b;
	@Before
	public void firstTest() throws IllegalMoveExcetion, IllegalPlayException {
		b=new Board();
		  b.setupTheObjects();
	
	}
	@Test
	public void firstPlayer() throws IllegalMoveExcetion, IllegalPlayException {
		ArrayList<Player> ar = Player.getThePlayers();
		String s[] = new String[ar.size()];
		for (int i = 0; i < s.length; i++) {
			s[i] =  ar.get(i).getName();
					
		}
		 b.startTheMove(ar.get(0));
			String i =  s[2];
			System.out.println(i);
			assertTrue(i.equals("Col"));
		

	}
	/*@Test
	public void nextPlayer() throws IllegalMoveExcetion, IllegalPlayException {
		ArrayList<Player> ar = Player.getThePlayers();
		 b.startTheMove(ar.get(0));
			String i =  b.name;
			assertTrue(i =="c");
		

	}*/
	
	

/*	// Tests that a move only Test that your project correctly identifies as
	// legal a move that only goes
	// horizontally for as many squares as the die roll;est
	@Test
	public void testHorizontalMove() throws IllegalMoveExcetion {
		ArrayList<Player> arr = new ArrayList<>();
		ArrayList<Player> p = Player.getThePlayers();
		
		 * for (int i = 0; i < p.size(); i++) {
		 * System.out.println(arr.get(0).getName()); }
		 
		// Player.setTokenPosition(8,0);
		// boolean isLegal = Player.isMoveLegal(4, arr.get(0));
		assertTrue(Player.isMoveLegal(4, arr.get(0)));
	}

	@Test
	public void testVerticalMove() {

	}

	@Test
	public void testMoveThatPassDieRoll() {

	}

	@Test
	public void testIllegalMoveThruDoor() {

	}

	@Test
	public void testMoveThruWall() {

	}

	@Test
	public void testMoveThatUseSecretPassage() {

	}

	@Test
	public void testMoveNotContinous() {

	}

	@Test
	public void testDiagonally() {

	}


/*
	@Test
	public void secondPlayer() {

	}

	@Test
	public void thirdPlayer() {

	}

	@Test
	public void fouthPlayer() {

	}

	@Test
	public void fithPlayer() {

	}*/
}
